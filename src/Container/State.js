import React, {Component} from 'react'
import {  StyleSheet, Text, View, Button, TouchableOpacity,TextInput  } from 'react-native'

class StateLatian extends Component{
    constructor(){
        super()
        this.state = {
            nama: 'Ario',
        }
    }
    buttonPress = () => {
        this.setState({
            nama: this.state.nama   
        })
    }
    change = () => {
        this.setState({
            nama: 'Budi'
        })
    }
    
    render () {
    return (
        <View style={styles.container}>
            <View style={styles.klik}>
                <Text style={{color: 'black', fontSize:40}}>{JSON.stringify(this.state)}</Text>                
                <Text style={{color:'black', fontWeight:'bold',fontSize:40}}>{this.state.nama}</Text>
                </View>
                <TouchableOpacity style={styles.button} onPress={this.buttonPress}>
                <Text style={{color:'black', fontSize: 40}}>Klik</Text>
                </TouchableOpacity>                      
        </View>
        )
    }
}
export default StateLatian;

const styles = StyleSheet.create({
    container:{
        flex:1,
        backgroundColor: 'yellow',
        alignItems: 'center',
        justifyContent:'center'
    },
    klik:{
        borderWidth:1,
        borderRadius:10
    },
    button: {borderWidth:1,
    borderRadius: 10,
    marginTop: 30,
    borderWidth: 2,
    backgroundColor: 'yellow',
    width: 100,
    alignItems: 'center',
    elevation: 10,}
})